import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMaps extends StatefulWidget {
  const GoogleMaps({
    Key key,
    this.cameraPosition,
    this.markers,
  }) : super(key: key);

  final CameraPosition cameraPosition;
  final Set<Marker> markers;

  @override
  _GoogleMapsState createState() => _GoogleMapsState(); 
}

class _GoogleMapsState extends State<GoogleMaps> {
  Completer<GoogleMapController> _controller = Completer();

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      onMapCreated: _onMapCreated,
      initialCameraPosition: widget.cameraPosition,
      markers: widget.markers,
    );
  }

}