import 'package:flutter/material.dart';

class FloatingSearch extends StatefulWidget {
  const FloatingSearch({
    Key key,
    this.scaffoldKey,
  }): super(key: key);

  final GlobalKey<ScaffoldState> scaffoldKey;
  @override
  _FloatingSearchState createState() => _FloatingSearchState();
}

class _FloatingSearchState extends State<FloatingSearch> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
          top: 10,
          right: 15,
          left: 15,
          child: Container(
            color: Colors.white,
            child: Row(
              children: <Widget>[
                IconButton(
                  splashColor: Colors.grey,
                  icon: Icon(Icons.menu),
                  onPressed: () {
                    widget.scaffoldKey.currentState.openDrawer();
                  },
                ),
                Expanded(
                  child: TextField(
                    cursorColor: Colors.black,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.go,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 15),
                        hintText: "Search..."),
                        onChanged: (val) {
                          setState(() {
                            print(val + ' ttttt');
                          });
                        },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: IconButton(icon: Icon(Icons.voice_chat), onPressed: null),
                ),
              ],
            ),
          ),
        );
  }
}