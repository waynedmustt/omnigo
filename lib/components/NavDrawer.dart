import 'package:flutter/material.dart';
import 'package:google_maps_in_flutter/models/menu.dart';
import 'package:google_maps_in_flutter/pages/payment/index.dart';
import 'package:google_maps_in_flutter/pages/setting/index.dart';

class NavDrawer extends StatefulWidget {
  const NavDrawer({
    Key key,
    this.menus,
  }): super(key: key);

  final List menus;

  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {

  final List<Widget> menuList = [];

  Widget getPage(value) {
    switch (value) {
      case 'payment':
        return Payment(title: 'Payment');
        break;
      case 'settings':
        return Settings(title: 'Settings');
        break;
      default:
    }

    return Payment(title: 'Payment');
  }

  List<Widget> getDrawer(BuildContext context) {
    // Add drawer header
    DrawerHeader menuHeader = DrawerHeader(
      child: Text('OmniGo'),
      decoration: BoxDecoration(
        color: Colors.blue,
      ),
    ); 
    menuList.add(menuHeader);

    // add menu List
    widget.menus.forEach((e) {
      menuList.add(
        ListTile(
          title: Text(Menu.fromJson(e).label),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => getPage(Menu.fromJson(e).value)),
            );
          },
        )
      );
    });

    return menuList;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: getDrawer(context),
        ),
      );
  }
}