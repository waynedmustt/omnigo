import 'package:flutter/material.dart';

class BottomNav extends StatefulWidget {
  const BottomNav({
    Key key,
  }): super(key: key);

  @override
  _FloatingSearchState createState() => _FloatingSearchState();
}

class _FloatingSearchState extends State<BottomNav> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
          bottom: 10,
          right: 15,
          left: 15,
          child: Container(
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: CircleAvatar(
                    backgroundColor: Colors.deepPurple,
                    child: Text('RD'),
                  ),
                ),
                RaisedButton(
                    color: Colors.blue,
                    child: Text('Find Nearby'),
                    onPressed: null),
                Container()
              ],
            ),
          ),
        );
  }
}