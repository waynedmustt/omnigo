import 'dart:convert';

List<Menu> modelUserFromJson(String str) => List<Menu>.from(json.decode(str).map((x) => Menu.fromJson(x)));
String modelUserToJson(List<Menu> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Menu {
  int key;
  String label;
  String title;
  String value;

  Menu(
    this.key, 
    this.label,
    this.title,
    this.value
    );

  factory Menu.fromJson(dynamic json) {
    return Menu(
      json['key'] as int, 
      json['label'] as String,
      json['title'] as String,
      json['value'] as String
    );
  }

  dynamic toJson() => {
    "key": key,
    "label": label,
    "title": title,
    "value": value,
  };
}