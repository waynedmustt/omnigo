import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_in_flutter/components/BottomNav.dart';
import 'package:google_maps_in_flutter/components/NavDrawer.dart';
import 'package:google_maps_in_flutter/components/floatingSearch.dart';
import 'package:google_maps_in_flutter/components/googleMaps.dart';

final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final Set<Marker> _markers = {};

  // Marcphenson Road Singapore.
  final CameraPosition initialCameraPosition = CameraPosition(
    target: LatLng(1.331412, 103.878910),
    zoom: 18.0,
  );
  // defined lat long marker
  final List<LatLng> markerLocations = [
    LatLng(1.331412, 103.878910),
    LatLng(1.331532, 103.879930),
  ];

  final List menus = [
    {
      'key': 1,
      'value': 'payment',
      'label': 'Payment',
      'title': 'Payment',
    },
    {
      'key': 2,
      'value': 'settings',
      'label': 'Settings',
      'title': 'Settings',
    }
  ];

  @override
  void initState() {
    // add new lat long marker
    markerLocations.add(
      LatLng(1.331232, 103.877930),
    );
    for (LatLng markerLocation in markerLocations) {
        _markers.add(
            Marker(
              markerId: MarkerId(markerLocations.indexOf(markerLocation).toString()),
              position: markerLocation,
              icon: BitmapDescriptor.defaultMarker,
            ),
        );
      }
    super.initState();
  }

  GoogleMaps _setGoogleMaps() {
    return GoogleMaps(
      cameraPosition: initialCameraPosition,
      markers: _markers,
    );
  }

  FloatingSearch _setFloatingSearch(GlobalKey _scaffoldKey) {
    return FloatingSearch(scaffoldKey: _scaffoldKey);
  }

  BottomNav _setBottomNav() {
    return BottomNav();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: _scaffoldKey,
        drawer: NavDrawer(menus: menus),
        appBar: AppBar(
          automaticallyImplyLeading: false, // this will hide Drawer hamburger icon
          actions: <Widget>[Container()],   // this will hide endDrawer hamburger icon
          title: Text(widget.title),
          backgroundColor: Colors.green[700],
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              _setGoogleMaps(),
              _setFloatingSearch(_scaffoldKey),
              _setBottomNav()
            ],
          ),
        ),
      ),
    );
  }

}