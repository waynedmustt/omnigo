import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  Settings({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            tooltip: 'Go Back',
            onPressed: () => {
              Navigator.pop(context)
            }, // null disables the button
          ),
          title: Text(widget.title),
          backgroundColor: Colors.green[700],
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              Container(
                child: Center(
                  child: Text('Settings!'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}